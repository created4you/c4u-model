<?php

namespace C4U\Model;

class Mapper {

	private $connection;
	private $extractor;
	private $tableName;

	protected $entityName;
	protected $idColumn = 'id';
	protected $defaultSort = 'DESC';

	public function __construct($tableName, \Dibi\Connection $connection, Extractor $extractor) {
		$this->tableName = $tableName;
		$this->connection = $connection;
		$this->extractor = $extractor;
	}

	/**
	 *
	 * @return \Dibi\Connection
	 */
	protected function getConnection() {
		return $this->connection;
	}

	protected function getTableName() {
		return $this->tableName;
	}

	protected function getExtractor() {
		return $this->extractor;
	}

	protected function mapOne($row) {
		return $this->mapOneToEntity($this->entityName, $row);
	}

	protected function mapList(array $rows) {
		return $this->mapListToEntity($this->entityName, $rows);
	}

	protected function mapOneToEntity($entityName, $row) {
		if (!$row) {
			return null;
		}
		$entity = new $entityName;
		foreach ($row as $property => $value) {
			$entity->$property = $value;
		}
		return $entity;
	}

	protected function mapListToEntity($entityName, array $rows) {
		$entities = array();
		foreach ($rows as $row) {
			$entities[] = $this->mapOneToEntity($entityName, $row);
		}
		return $entities;
	}

	public function loadOneById($id) {
		return $this->mapOne($this->loadOneByCond(array($this->idColumn => $id)));
	}

	public function loadOneByCond(array $where) {
		return $this->mapOne($this->getConnection()->query('SELECT * FROM %n WHERE %and', $this->getTableName(), $where)->fetch());
	}

	public function loadAll($customOrderBy = null) {
		$orderBy = $customOrderBy ? $customOrderBy : array($this->idColumn => $this->defaultSort);
		return $this->mapList($this->getConnection()->query('SELECT * FROM %n ORDER BY %by', $this->getTableName(), $orderBy)->fetchAll());
	}

	public function loadAllByCond(array $where, $customOrderBy = null) {
		$orderBy = $customOrderBy ? $customOrderBy : array($this->idColumn => $this->defaultSort);
		return $this->mapList($this->getConnection()->query('SELECT * FROM %n WHERE %and ORDER BY %by', $this->getTableName(), $where, $orderBy)->fetchAll());
	}

	public function getLastId() {
		return $this->getConnection()->getInsertId();
	}

	public function insert($entity) {
		$properties = $this->extractor->extractProperties($entity);
		$this->getConnection()->query('INSERT INTO %n', $this->tableName, '%v', $properties);
		return $this->getLastId();
	}

	public function multipleReplace(array $entities, $chunks = 500) {
		foreach (array_chunk($entities, $chunks) as $chunk) {
			$mapped = $this->mapChunkToEntity($chunk);
			$this->getConnection()->query('REPLACE INTO %n', $this->tableName, '%ex', $mapped);
		}
	}

	public function multipleInsert(array $entities, $chunks = 500) {
		foreach (array_chunk($entities, $chunks) as $chunk) {
			$mapped = $this->mapChunkToEntity($chunk);
			$this->getConnection()->query('INSERT INTO %n', $this->tableName, '%ex', $mapped);
		}
	}

	public function mapChunkToEntity(array $chunk) {
		$output = array();
		foreach ($chunk as $entity) {
			$output[] = $this->extractor->extractProperties($entity);
		}
		return $output;
	}

	public function update($entity, array $where = null) {
		if ($where === null) $where = array($this->idColumn => $entity->{$this->idColumn});

		$properties = $this->extractor->extractProperties($entity);
		$this->getConnection()->query('UPDATE %n SET', $this->tableName, $properties, 'WHERE %a', $where);
	}

	public function delete(array $where) {
		$this->getConnection()->query('DELETE FROM %n', $this->tableName, 'WHERE %and', $where);
	}

	public function getArray($entity) {
		return $this->extractor->extractProperties($entity);
	}

	public function startTransaction() {
		$this->getConnection()->begin();
	}

	public function commitTransaction() {
		$this->getConnection()->commit();
	}

	public function rollbackTransaction() {
		$this->getConnection()->rollback();
	}

	public function loadAllById($id) {
		return $this->mapList($this->loadAllByCond(array($this->idColumn => $id)));
	}

	public function loadGridSourceBase() {
		return $this->getConnection()->select('*')->from(
			'%n', $this->getTableName()
		);
	}

}
