<?php

namespace C4U\Model;

class Extractor {

	public function extractProperties(Entity $entity) {
		$reflection = new \ReflectionClass($entity);
		$props = $reflection->getProperties();

		$propsArray = array();
		foreach ($props as $prop) {
			$propsArray[$prop->name] = $entity->{$prop->name} !== '' ? $entity->{$prop->name} : null;
		}

		return $propsArray;
	}

}
