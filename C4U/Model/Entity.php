<?php

namespace C4U\Model;

abstract class Entity {

	private $reflectionInstance = null;

	public function __set($name, $value) {
		if ($this->hasProperty($name)) {
			$this->$name = $value;
		} else {
			$this->handleMissingProperty($name);
		}
	}

	public function __get($name) {
		if ($this->hasProperty($name)) {
			return $this->$name;
		} else {
			$this->handleMissingProperty($name);
		}
	}

	public function __isset($name) {
		return $this->hasProperty($name) && $this->$name !== null;
	}

	public function __unset($name) {
		if ($this->hasProperty($name)) {
			$this->$name = null;
		} else {
			$this->handleMissingProperty($name);
		}
	}

	private function getReflectionObject() {
		if ($this->reflectionInstance === null) {
			$this->reflectionInstance = new \ReflectionObject($this);
		}
		return $this->reflectionInstance;
	}

	private function hasProperty($name) {
		$obj = $this->getReflectionObject();
		return $obj->hasProperty($name);
	}

	private function handleMissingProperty($name) {
		// Do nothning.
		// throw new \InvalidArgumentException('Entity has not a property with name ' . $name);
	}

}
