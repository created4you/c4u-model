<?php

namespace C4U\ModelGenerator;

use C4U\Components\Template\TemplateFactory;
use C4U\ModelGenerator\Entity\EntityGenerator;
use C4U\ModelGenerator\Mapper\MapperGenerator;
use Dibi\Connection;

class ModelGenerator {

	protected $connection;
	protected $databaseName;
	protected $path;
	protected $templateFactory;

	public function __construct(
		$databaseName,
		$path,
		Connection $connection,
		TemplateFactory $templateFactory
	) {
		$this->databaseName = $databaseName;
		$this->connection = $connection;
		$this->path = $path;
		$this->templateFactory = $templateFactory;
	}

	public function generateEntities($tableName, TemplateFactory $templateFactory) {
		$entityGenerator = new EntityGenerator($this->databaseName, $this->path, $this->connection, $templateFactory);
		$entityGenerator->render($tableName);
	}

	public function generateMappers($tableName, TemplateFactory $templateFactory) {
		$mapperGenerator = new MapperGenerator($this->databaseName, $this->path, $this->connection, $templateFactory);
		$mapperGenerator->render($tableName);
	}

	protected function sanitizeTableName($tableName) {
		return ucfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($tableName)))));
	}

	protected function getDataType($dibiDataType) {
		switch ($dibiDataType) {
			case 'varchar':
			case 'text':
			case 'varchar': return 'string';
		}
		return $dibiDataType;
	}

}