<?php

namespace C4U\ModelGenerator\Mapper;

use C4U\ModelGenerator\ModelGenerator;
use Nette\Latte\Engine;
use Nette\Templating\FileTemplate;

class MapperGenerator extends ModelGenerator{
	
	public function render($tableName) {
		$template = $this->templateFactory->createTemplate(dirname(__FILE__) . '/default.phtml');
		$template->addFilter('getDataType', [$this, 'getDataType']);
		$template->tableName = $this->sanitizeTableName($tableName);

		$tableName = $this->sanitizeTableName($tableName);

		$file = fopen($this->path . '/Mapper/' . ucfirst($tableName) . 'Mapper.php', 'w');
		fputs($file, (string)$template);
		fclose($file);
	}

	/* @override */
	public function getDataType($dibiDataType) {
		parent::getDataType($dibiDataType);
	}

}
