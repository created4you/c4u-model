<?php

namespace C4U\ModelGenerator\Service;

use C4U\ModelGenerator\ModelGenerator;
use Nette\Latte\Engine;
use Nette\Templating\FileTemplate;

class ServiceGenerator extends ModelGenerator{
	
	public function render($tableName) {
		$cols = $this->connection->query(
			'SELECT column_name, data_type FROM information_schema.`COLUMNS`',
			'WHERE TABLE_NAME = %s AND TABLE_SCHEMA = %s', $tableName, $this->databaseName
		)->fetchAll();

		$template = new FileTemplate();
		$template->registerHelperLoader('Nette\Templating\Helpers::loader');
		$template->registerHelper('getDataType', callback($this, 'getDataType'));
		$template->registerFilter(new Engine());
		$template->setFile(dirname(__FILE__) . '/default.phtml');
		$template->tableName = $this->sanitizeTableName($tableName);
		$template->cols = $cols;

		$tableName = $this->sanitizeTableName($tableName);

		$file = fopen(__DIR__ . '/../../../../app/model/Entity/' . ucfirst($tableName) . '.php', 'w');
		fputs($file, (string)$template);
		fclose($file);
	}

	/* @override */
	public function getDataType($dibiDataType) {
		echo 'here';
		parent::getDataType($dibiDataType);
	}

}
