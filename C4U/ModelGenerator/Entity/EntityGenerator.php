<?php

namespace C4U\ModelGenerator\Entity;

use C4U\ModelGenerator\ModelGenerator;
use Latte\Runtime\Template;
use Nette\Latte\Engine;
use Nette\Templating\FileTemplate;

class EntityGenerator extends ModelGenerator{
	
	public function render($tableName) {
		$cols = $this->connection->query(
			'SELECT column_name, data_type FROM information_schema.`COLUMNS`',
			'WHERE TABLE_NAME = %s AND TABLE_SCHEMA = %s', $tableName, $this->databaseName
		)->fetchAll();

		$template = $this->templateFactory->createTemplate(dirname(__FILE__) . '/default.phtml');
		$template->addFilter('getDataType', [$this, 'getDataType']);
		$template->tableName = $this->sanitizeTableName($tableName);
		$template->cols = $cols;

		$tableName = $this->sanitizeTableName($tableName);

		$file = fopen($this->path . '/Entity/' . ucfirst($tableName) . '.php', 'w');
		fputs($file, (string)$template);
		fclose($file);
	}

	/* @override */
	public function getDataType($dibiDataType) {
		parent::getDataType($dibiDataType);
	}

}
